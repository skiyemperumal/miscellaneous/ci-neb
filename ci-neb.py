#Description: This script generates initial structures for 
#various images based on different interpolation scheme
#---------------------------------------------------
# Required Files: initial.xyz and final.xyz 
# Required Packages: ASE 
# Usage: python ci-neb.py 
#
# Output: 
#         n.xyz: corresponding from 1 to n images (including end points) 
#         movie.xyz: an xyz movie file from 1 to n images
#---------------------------------------------------
# Author: Satish Kumar Iyemperumal e-mail: satish2414 [at] gmail.com
# Date:   April 03, 2018 
#---------------------------------------------------

from ase.io import read, write
from ase.neb import NEB
import os

initial = read('initial.xyz')
final = read('final.xyz')
n_images = int(raw_input("Enter number of images excluding end points"))

images = [initial]
for i in range(n_images):
    image = initial.copy()
    images.append(image)

images.append(final)
neb = NEB(images)
#Remove idpp below for linear interpolation
neb.interpolate('idpp')

for n, neb_atom in enumerate(neb.images):
    write('%d.xyz'%(n+1),neb_atom)
    os.system("cat %d.xyz >> movie.xyz"%(n+1))

